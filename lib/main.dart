import 'package:flutter/material.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/pages/login_page.dart';
import 'package:my_movies_list/ui/pages/register_page.dart';
import 'package:my_movies_list/ui/pages/search_page.dart';
import 'package:my_movies_list/ui/pages/spash_page.dart';
import 'package:my_movies_list/ui/pages/title_details_page.dart';
import 'package:my_movies_list/ui/pages/titles_rated_page.dart';

void main() {
  setupLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Movies List',
      debugShowCheckedModeBanner: false,
      initialRoute: SplashPage.name,
      routes: {
        LoginPage.name: (_) => const LoginPage(),
        HomePage.name: (_) => const HomePage(),
        SearchPage.name: (_) => SearchPage(),
        TitleDetailsPage.name: (_) => const TitleDetailsPage(),
        RegisterPage.name: (_) => const RegisterPage(),
        TitlesRatedPage.name: (_) => TitlesRatedPage(),
        SplashPage.name: (_) => const SplashPage(),
      },
    );
  }
}
