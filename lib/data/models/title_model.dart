class TitleModel {
  int id;
  String name;
  String coverUrl;
  String posterUrl;
  bool isTvShow;
  int? rate;

  TitleModel({
    required this.id,
    required this.name,
    required this.coverUrl,
    required this.posterUrl,
    required this.isTvShow,
    this.rate,
  });

  factory TitleModel.fromJson(Map<String, dynamic> json) {
    return TitleModel(
      id: json['id'],
      name: json['name'],
      coverUrl: json['cover_url'],
      posterUrl: json['poster_url'],
      isTvShow: json['is_tv_show'],
      rate: json['rate'],
    );
  }
}
