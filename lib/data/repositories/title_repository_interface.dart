import 'package:my_movies_list/data/models/title_details_model.dart';
import 'package:my_movies_list/data/models/title_model.dart';

abstract class TitleRepositoryInterface {
  Future<List<TitleModel>> getMovieList({Map<String, dynamic>? params});

  Future<TitleDetailModel?> getTitleDetails(int id, {bool isTvShow = false});

  Future<List<TitleModel>> getTvList({Map<String, dynamic>? params});

  Future<List<TitleModel>> getTitleRecommendations(int id, {bool isTvShow = false});

  Future<bool> saveComment(int titleId, String text, {bool isTvShow = false});

  Future<bool> removeComment(int titleId, int commentId, {bool isTvShow = false});
  
  Future<bool> saveTitleRate(int titleId, int rate, {bool isTvShow = false});

  Future<int?> getTitleRate(int titleId, {bool isTvShow = false});

  Future<List<TitleModel>> getTitlesRated(String userId);

  Future<List<CommentModel>> getTitleComments(int id, {isTvShow = false});
}
