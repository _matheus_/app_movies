import 'package:my_movies_list/data/models/title_details_model.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/data/services/http_service.dart';
import 'package:my_movies_list/shared/strings.dart';

class TitleRepository implements TitleRepositoryInterface {
  final HttpService _httpService;
  final _baseUrl = Strings.movie_api_url;
  final UserRepositoryInterface _userRepository;

  TitleRepository(this._httpService, this._userRepository);

  @override
  Future<List<TitleModel>> getMovieList({Map<String, dynamic>? params}) async {
    final uri = '$_baseUrl/movies';
    var response = await _httpService.getRequest(uri, params: params);
    if (response.success) {
      return _buildTitleModelList(response.content!['data']);
    }

    return [];
  }

  @override
  Future<TitleDetailModel?> getTitleDetails(int id,
      {bool isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$titleType/$id';
    var response = await _httpService.getRequest(uri);

    if (response.success) {
      return TitleDetailModel.fromJson(response.content!);
    }

    return null;
  }

  @override
  Future<List<TitleModel>> getTvList({Map<String, dynamic>? params}) async {
    final uri = '$_baseUrl/tv';
    var response = await _httpService.getRequest(uri, params: params);
    if (response.success) {
      return _buildTitleModelList(response.content!['data']);
    }

    return [];
  }

  @override
  Future<List<TitleModel>> getTitleRecommendations(int id,
      {bool isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$titleType/$id/recommendations';

    var response = await _httpService.getRequest(uri);
    if (response.success) {
      return _buildTitleModelList(response.content!['data']);
    }

    return [];
  }

  @override
  Future<bool> removeComment(int titleId, int commentId,
      {bool isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$titleType/$titleId/$commentId/comment';
    final result = await _httpService.deleteRequest(uri);

    return result.success;
  }

  @override
  Future<bool> saveComment(int titleId, String text,
      {bool isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$titleType/$titleId/comment';
    final result = await _httpService.postRequest(uri, {'comment': text});

    return result.success;
  }

  @override
  Future<int?> getTitleRate(int titleId, {bool isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$titleType/$titleId/rate';
    final token = await _userRepository.getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final result = await _httpService.getRequest(uri, headers: headers);

    if (result.success) {
      return result.content!['rate'];
    }
  }

  @override
  Future<bool> saveTitleRate(int titleId, int rate,
      {bool isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$titleType/$titleId/rate';
    final token = await _userRepository.getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final body = {'rate': rate};
    final result = await _httpService.postRequest(uri, body, headers: headers);

    return result.success;
  }

  @override
  Future<List<TitleModel>> getTitlesRated(String userId) async {
    final uri = '$_baseUrl/users/$userId/titles-rated';
    final token = await _userRepository.getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final response = await _httpService.getRequest(uri, headers: headers);

    if (response.success) {
      return _buildTitleModelList(response.content!['data']);
    }

    return [];
  }

  @override
  Future<List<CommentModel>> getTitleComments(int id,
      {isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$titleType/$id/comments';
    final response = await _httpService.getRequest(uri);

    if (response.success) {
      return response.content!['data'].map((j) => CommentModel.fromJson(j));
    }

    return [];
  }

  List<TitleModel> _buildTitleModelList(List<dynamic> data) {
    return List<TitleModel>.from(data.map((j) => TitleModel.fromJson(j)));
  }
  
}
