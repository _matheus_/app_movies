import 'package:flutter/material.dart';

class Carousel extends StatelessWidget {
  final String? label;
  final List<Widget> children;

  const Carousel({
    this.label,
    required this.children,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label ?? '',
          style: TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.w700,
            color: Theme.of(context).primaryColorDark,
          ),
        ),
        const SizedBox(height: 10.0),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: children,
          ),
        )
      ],
    );
  }
}
