import 'package:flutter/material.dart';
import 'package:my_movies_list/ui/widgets/custom_network_image.dart';

class TitleThumbnailModel {
  String name;
  String imageUrl;

  TitleThumbnailModel({required this.name, required this.imageUrl});
}

class TitleThumbnail extends StatelessWidget {
  final TitleThumbnailModel title;
  final Color backgroundColor;
  final double? width;
  final double? height;
  const TitleThumbnail({
    required this.title,
    this.width = 160.0,
    this.height,
    this.backgroundColor = Colors.blue,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(4.0),
      width: width,
      color: backgroundColor,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 4.0),
            child: CustomNetworkImage(
              url: title.imageUrl,
              height: height,
              width: width,
            ),
          ),
          Container(
            width: width,
            color: Theme.of(context).primaryColor,
            padding: const EdgeInsets.only(left: 8.0, bottom: 4.0, top: 4.0),
            child: Center(
              child: Text(
                title.name,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                  color: Colors.white60,
                  fontSize: 13.0,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
