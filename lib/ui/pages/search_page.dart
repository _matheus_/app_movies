import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/ui/widgets/title_thumbnail.dart';

class SearchPage extends StatelessWidget {
  static const name = 'search-page';
  //final _repository = TitleRepository();

  SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextFormField(
          style: const TextStyle(color: Colors.white),
          decoration: const InputDecoration(
            hintText: 'Pesquise aqui...',
            hintStyle: TextStyle(color: Colors.white30),
            border: InputBorder.none,
          ),
        ),
      ),
      body: GridView.count(
        padding: const EdgeInsets.only(top: 8.0),
        mainAxisSpacing: 20.0,
        crossAxisCount: 2,
        children: []
            //_repository.getTitleList().map((t) => _buildTitleCard(t)).toList(),
      ),
    );
  }

  Widget _buildTitleCard(TitleModel title) {
    return Center(
      child: TitleThumbnail(
        height: 160.0,
        title: TitleThumbnailModel(name: title.name, imageUrl: title.posterUrl),
      ),
    );
  }
}
