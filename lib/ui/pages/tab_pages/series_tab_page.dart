import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/blocs/catalog_title_cubit.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/tab_pages/catalog_title_view.dart';

class SeriesTabPage extends StatelessWidget {
  const SeriesTabPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final titlesList = [
      TitleType('Série de comédia', {'genre': 35}, true),
      TitleType('Série de thriller', {'genre': 53}, true),
    ];

    return BlocProvider(
      create: (_) => CatalogTitleCubit(getIt.get<TitleRepositoryInterface>())
        ..fetchTitles(titlesList),
      child: CatalogTitleView(titlesList),
    );
  }
}
