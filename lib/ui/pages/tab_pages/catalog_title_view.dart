import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/blocs/catalog_title_cubit.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/ui/pages/title_details_page.dart';
import 'package:my_movies_list/ui/widgets/carousel.dart';
import 'package:my_movies_list/ui/widgets/title_thumbnail.dart';

class CatalogTitleView extends StatelessWidget {
  final List<TitleType> titlesList;

  const CatalogTitleView(this.titlesList, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: titlesList
            .map(
              (t) => BlocBuilder<CatalogTitleCubit, TitleState>(
                buildWhen: (oldState, newState) {
                  if (newState is LoadingTitlesState) {
                    return newState.type.label == t.label;
                  } else if (newState is TitlesLoadedState) {
                    return newState.type.label == t.label;
                  }

                  return true;
                },
                builder: (context, state) {
                  if (state is LoadingTitlesState) {
                    return const CircularProgressIndicator();
                  } else if (state is TitlesLoadedState) {
                    return Carousel(
                      label: t.label,
                      children: state.titles
                          .map((t) =>
                              _buildTitleCard(title: t, context: context))
                          .toList(),
                    );
                  }

                  return Container();
                },
              ),
            )
            .toList(),
      ),
    );

    // return Padding(
    //   padding: const EdgeInsets.all(8.0),
    //   child: Column(
    //     children: [
    //       FutureBuilder<List<TitleModel>>(
    //           future: _repository.getMovieList(params: {'genre': 16}),
    //           builder: (context, snapshot) {
    //             if (snapshot.connectionState == ConnectionState.waiting) {
    //               return const Text('Carregando');
    //             }

    //             final movies = snapshot.data;

    //             return Carousel(
    //               label: 'Filmes de ação',
    //               children: movies!
    //                   .map((t) => _buildTitleCard(title: t, context: context))
    //                   .toList(),
    //             );
    //           }),
    //     ],
    //   ),
    // );
  }

  Widget _buildTitleCard(
      {required TitleModel title, required BuildContext context}) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, TitleDetailsPage.name, arguments: {
          'id': title.id,
          'isTvShow': title.isTvShow,
        });
      },
      child: GestureDetector(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: TitleThumbnail(
            width: 120.0,
            title: TitleThumbnailModel(
                name: title.name, imageUrl: title.posterUrl),
          ),
        ),
      ),
    );
  }
}
