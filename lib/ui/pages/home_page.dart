import 'package:flutter/material.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/login_page.dart';
import 'package:my_movies_list/ui/pages/search_page.dart';
import 'package:my_movies_list/ui/pages/tab_pages/main_tab_page.dart';
import 'package:my_movies_list/ui/pages/tab_pages/movies_tab_page.dart';
import 'package:my_movies_list/ui/pages/tab_pages/series_tab_page.dart';
import 'package:my_movies_list/ui/pages/titles_rated_page.dart';

class HomePage extends StatelessWidget {
  static const name = 'home-page';

  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Meu catálogo de filmes e séries'),
          bottom: const TabBar(
            tabs: [
              Tab(child: Text('Principal')),
              Tab(child: Text('Filmes')),
              Tab(child: Text('Séries')),
            ],
          ),
          actions: [
            IconButton(
              onPressed: () {
                Navigator.pushNamed(context, SearchPage.name);
              },
              icon: const Icon(Icons.search),
            ),
            IconButton(
              onPressed: () {
                Navigator.pushNamed(context, TitlesRatedPage.name);
              },
              icon: const Icon(Icons.account_circle_sharp),
            ),
            IconButton(
              onPressed: () async {
                await getIt.get<UserRepositoryInterface>().clearSession();
                Navigator.pushNamedAndRemoveUntil(
                    context, LoginPage.name, (context) => false);
              },
              icon: const Icon(Icons.exit_to_app),
            ),
          ],
        ),
        body: TabBarView(
          physics: const NeverScrollableScrollPhysics(),
          children: [
            MainTabPage(),
            const MoviesTabPage(),
            const SeriesTabPage(),
          ],
        ),
      ),
    );
  }
}
