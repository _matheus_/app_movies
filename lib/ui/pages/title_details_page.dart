import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/blocs/title_details/comments_cubit.dart';
import 'package:my_movies_list/blocs/title_details/rate_cubit.dart';
import 'package:my_movies_list/data/models/title_details_model.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/widgets/carousel.dart';
import 'package:my_movies_list/ui/widgets/custom_network_image.dart';
import 'package:my_movies_list/ui/widgets/custom_text_field.dart';
import 'package:my_movies_list/ui/widgets/loading_button.dart';
import 'package:my_movies_list/ui/widgets/rate_buttons.dart';

class TitleDetailsPage extends StatelessWidget {
  static const name = 'title-details-page';
  const TitleDetailsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)!.settings.arguments as Map;
    int titleId = arguments['id'] as int;
    bool isTvShow = arguments['isTvShow'] as bool;

    return MultiBlocProvider(
      providers: [
        BlocProvider<RateCubit>(
          create: (_) => RateCubit(getIt.get<TitleRepositoryInterface>())
            ..getRate(titleId, isTvShow),
        ),
        BlocProvider<CommentCubit>(
          create: (_) => CommentCubit(getIt.get<TitleRepositoryInterface>())
            ..loadComments(titleId, isTvShow),
        )
      ],
      child: TitleDetailsView(titleId: titleId, isTvShow: isTvShow),
    );
  }
}

class TitleDetailsView extends StatelessWidget {
  final int titleId;
  final bool isTvShow;
  final _repository = getIt.get<TitleRepositoryInterface>();
  final _commentController = TextEditingController();

  TitleDetailsView({required this.titleId, required this.isTvShow, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            _buildDetailsSection(titleId, isTvShow),
            _buildRateSection(),
            _buildRecommendationsSection(titleId, isTvShow),
            _buildCommentsSection()
          ],
        ),
      ),
    );
  }

  Widget _buildDetailsSection(int titleId, bool isTvShow) {
    return FutureBuilder<TitleDetailModel?>(
      future: _repository.getTitleDetails(titleId, isTvShow: isTvShow),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }

        if (!snapshot.hasData) {
          return const Text('Falha o carregar detalhes');
        }

        final title = snapshot.data!;

        return SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomNetworkImage(url: title.coverUrl),
              Column(
                children: [
                  Text(
                    title.name +
                        (title.releaseDate == null
                            ? ''
                            : '(${title.releaseDate!.year})'),
                    style: const TextStyle(
                        fontWeight: FontWeight.w500, fontSize: 18.0),
                  ),
                  const SizedBox(height: 20.0),
                  const Text(
                    'Sinopse:',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  const SizedBox(height: 5.0),
                  Text(
                    title.overview,
                    style: const TextStyle(fontSize: 15.0),
                  ),
                  const SizedBox(height: 10.0),
                  Text('Duração: ${title.runtime} min'),
                  const SizedBox(height: 20.0),
                  Wrap(
                    children: title.genres
                        .map((g) => Padding(
                              padding: const EdgeInsets.only(left: 4.0),
                              child: Chip(label: Text(g)),
                            ))
                        .toList(),
                  ),
                ],
              )
            ],
          ),
        );
      },
    );
  }

  Widget _buildRateSection() {
    return BlocBuilder<RateCubit, RateState>(
      builder: (context, state) {
        if (state is RateLoadedState) {
          return RateButton(
            value: state.rate,
            onChanged: (value) {
              context.read<RateCubit>().saveRate(titleId, value);
            },
          );
        }

        return Container();
      },
    );
  }

  Widget _buildRecommendationsSection(int titleId, bool isTvShow) {
    return FutureBuilder<List<TitleModel>>(
      future: _repository.getTitleRecommendations(titleId, isTvShow: isTvShow),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }

        if (snapshot.data!.isNotEmpty) {
          final movies = snapshot.data!;
          final carouselItems = movies
              .map((m) => CustomNetworkImage(url: m.posterUrl, height: 150.0))
              .toList();
          return Carousel(
            children: carouselItems,
          );
        }

        return Container();
      },
    );
  }

  Widget _buildCommentsSection() {
    return Column(children: [
      const Text(
        'Comentários',
        style: TextStyle(
          fontSize: 18.0,
        ),
      ),
      const SizedBox(height: 15.0),
      Row(
        children: [
          Expanded(
            child: CustomTextField(
              label: 'Comentar',
              controller: _commentController,
            ),
          ),
          LoadingButton(
            child: const Text('Enviar'),
            onPressed: () async {
              // await _repository.saveComment(
              //   title.id,
              //   _commentController.text,
              //   isTvShow: isTvShow,
              // );
              _commentController.clear();
            },
          ),
        ],
      ),
      BlocBuilder<CommentCubit, CommentState>(
        builder: (context, state) {
          if (state is CommentsLoadedState) {
            return Column(
              children:
                  state.comments.map((c) => _buildCommentsItem(c)).toList(),
            );
          }

          return Container();
        },
      )
    ]);
  }

  Widget _buildCommentsItem(CommentModel comment) {
    return ListTile(
      title: Text(comment.text),
      subtitle: Text(comment.date.toString()),
      trailing: IconButton(
        onPressed: () async {
          //await _repository.removeComment(title.id, c.id);
        },
        icon: const Icon(Icons.remove),
      ),
    );
  }
}
