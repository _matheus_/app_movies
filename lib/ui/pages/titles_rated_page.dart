import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository.dart';
import 'package:my_movies_list/ui/widgets/custom_network_image.dart';
import 'package:my_movies_list/ui/widgets/rate_buttons.dart';
import 'package:my_movies_list/ui/widgets/title_thumbnail.dart';

class TitlesRatedPage extends StatelessWidget {
  static const name = 'titles-rated-page';

  TitlesRatedPage({Key? key}) : super(key: key);

  //final _repository = TitleRepository();

  @override
  Widget build(BuildContext context) {
    final titles = [];//_repository.getTitleList();
    return Scaffold(
        appBar: AppBar(
          title: const Text('Filmes e séries classificados'),
        ),
        body: ListView.builder(
          itemCount: titles.length,
          itemBuilder: (context, index) {
            return _buildTitleCard(context, titles[index]);
          },
        ));
  }

  Widget _buildTitleCard(BuildContext context, TitleModel title) {
    return Container(
      padding: const EdgeInsets.all(5.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CustomNetworkImage(
            height: 120.0,
            url: title.posterUrl,
          ),
          const SizedBox(width: 5.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title.name,
                style: TextStyle(
                  color: Theme.of(context).primaryColorDark,
                  fontSize: 18.0,
                ),
              ),
              const RateButton(value: false),
            ],
          )
        ],
      ),
    );
  }
}
