import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/blocs/register_cubit.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/widgets/custom_text_field.dart';
import 'package:my_movies_list/ui/widgets/loading_button.dart';

class RegisterPage extends StatelessWidget {
  static const name = 'register-page';

  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<RegisterCubit>(
      create: (_) => RegisterCubit(getIt.get<UserRepositoryInterface>()),
      child: RegisterView(),
    );
  }
}

class RegisterView extends StatelessWidget {
  RegisterView({Key? key}) : super(key: key);

  final _emailController = TextEditingController();
  final _nameController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterCubit, RegisterState>(
      listener: (context, state) {
        if (state == RegisterState.success) {
          Navigator.pushReplacementNamed(context, HomePage.name);
        }
      },
      child: SafeArea(
        child: Scaffold(
          body: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Text(
                      'Informe seus dados',
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w300,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                  const SizedBox(height: 25.0),
                  CustomTextField(
                    label: 'Nome',
                    icon: Icons.account_box,
                    controller: _nameController,
                  ),
                  const SizedBox(height: 25.0),
                  CustomTextField(
                    label: 'Email',
                    icon: Icons.email,
                    keyBoardType: TextInputType.emailAddress,
                    controller: _emailController,
                  ),
                  const SizedBox(height: 25.0),
                  CustomTextField(
                    label: 'Senha',
                    icon: Icons.lock,
                    obscureText: true,
                    controller: _passwordController,
                  ),
                  const SizedBox(height: 15.0),
                  BlocBuilder<RegisterCubit, RegisterState>(
                      builder: (context, state) {
                    return Visibility(
                      visible: state == RegisterState.failed ||
                          state == RegisterState.userAlreadyExists,
                      child: Text(
                        state == RegisterState.userAlreadyExists
                            ? 'Email já está em uso'
                            : 'Falha ao realizar cadastro',
                        style: const TextStyle(color: Colors.red),
                      ),
                    );
                  }),
                  BlocBuilder<RegisterCubit, RegisterState>(
                      builder: (context, state) {
                    return LoadingButton(
                      isLoading: state == RegisterState.processing,
                      onPressed: () {
                        context.read<RegisterCubit>().register(
                              _nameController.text,
                              _emailController.text,
                              _passwordController.text,
                            );
                      },
                      child: const Text('Enviar'),
                    );
                  }),
                  const SizedBox(height: 15.0),
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text('Cancelar'),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
