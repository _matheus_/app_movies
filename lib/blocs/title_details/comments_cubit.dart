import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/models/title_details_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';

class CommentState {}

class LoadingCommentsState extends CommentState {}

class CommentsLoadedState extends CommentState {
  final List<CommentModel> comments;

  CommentsLoadedState(this.comments);
}

class CommentCubit extends Cubit<CommentState> {
  final TitleRepositoryInterface _titleRepository;

  CommentCubit(this._titleRepository) : super(CommentState());

  Future<void> loadComments(int titleId, bool isTvShow) async {
    emit(LoadingCommentsState());
    final comments =
        await _titleRepository.getTitleComments(titleId, isTvShow: isTvShow);
    emit(CommentsLoadedState(comments));
  }
}
