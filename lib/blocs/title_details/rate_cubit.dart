import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';

abstract class RateState {}

class RateInitialState extends RateState {}

class LoadingRateState extends RateState {}

class RateLoadedState extends RateState {
  final bool? rate;

  RateLoadedState(this.rate);
}

class SavingRateState extends RateState {}

class RateSavedState extends RateState {}

class RateCubit extends Cubit<RateState> {
  final TitleRepositoryInterface _titleRepository;

  RateCubit(this._titleRepository) : super(RateInitialState());

  Future<void> getRate(int titleId, bool isTvShow) async {
    emit(LoadingRateState());

    final rate =
        await _titleRepository.getTitleRate(titleId, isTvShow: isTvShow);

    if (rate != null) {
      emit(RateLoadedState(rate == 1));
    } else {
      emit(RateLoadedState(null));
    }
  }

  Future<void> saveRate(int titleId, bool rate) async {
    emit(SavingRateState());
    await _titleRepository.saveTitleRate(titleId, rate ? 1 : -1);
    emit(RateSavedState());
  }
}
