import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/exceptions/user_already_exists.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';

enum RegisterState { initial, processing, success, userAlreadyExists, failed }

class RegisterCubit extends Cubit<RegisterState> {
  final UserRepositoryInterface _userRepository;

  RegisterCubit(this._userRepository) : super(RegisterState.initial);

  Future<void> register(String name, String email, String password) async {
    emit(RegisterState.processing);
    try {
      await _userRepository.register(email, password, name);
      emit(RegisterState.success);
    } on UserAlredyExistsException {
      emit(RegisterState.userAlreadyExists);
    } on Exception {
      emit(RegisterState.failed);
    }
  }
}
